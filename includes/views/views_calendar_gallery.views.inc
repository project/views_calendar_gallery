<?php

/**
 * @file
 * Contains Views module hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function views_calendar_gallery_views_plugins() {
  $path = drupal_get_path('module', 'views_calendar_gallery');
  $plugins = array(
    'module' => 'views_calendar_gallery',
    'style' => array(
      'views_calendar_gallery_style' => array(
        'title' => t('Calendar Gallery'),
        'help' => t('Displays items on a calendar.'),
        'handler' => 'views_calendar_gallery_plugin_style',
        'path' => $path . '/includes/views',
        'theme' => 'views_calendar_gallery_style',
        'theme file' => 'theme.inc',
        'theme path' => $path . '/theme',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'even empty' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
  return $plugins;
}
