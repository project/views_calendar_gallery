<?php

/**
 * @file
 * Contains the default style plugin.
 */

class views_calendar_gallery_plugin_style extends views_plugin_style {
  /**
   * Set default options
   */
	/*
  function options(&$options) {
    parent::options($options);
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }*/
	/*
  function option_definition() {
    $options = parent::option_definition();
    $options['format'] = array('default' => 'Exhibit');

    return $options;
  }*/
  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);
  }
  /**
   * Set default options
   */
	public function option_definition() {
    	$options = parent::option_definition();
    	return $options;
	}

  	public function render() {
    	$output = '';
	    // Prevent errors if users select content instead of fields as row style.
	    // This forces them to use field as row style, no other way that I can think of.
	    if (parent::uses_fields()) {
	      $output = parent::render();
	    }
	    else {
	      drupal_set_message('Views accordion requires Fields as row style', 'error');
    	}
 
    	return $output;
  	}
/*
  function options(&$options) {
  	parent::options_form($form, $form_state);
    $options['height'] = array('default' => '300px');
    $options['width'] = array('default' => '100%');
    $options['title_main'] = array('default' => 'My Timeline');
    return $options;  // Not necessary to return or pass by reference? Users call on what they want to do
  }
  */
  
  /**
   * Create forms to hold these values allowing the user to change the values
   */
  function options_form(&$form, &$form_state) {
  	parent::options_form($form, $form_state);
  	/*
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => '30',
      '#description' => t('This field determines how tall the timeline will be'),
      '#default_value' => $this->options['height'],
    );
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => '30',
      '#description' => t('This field determines how wide the timeline will be'),
      '#default_value' => $this->options['width'],   
    );
    $form['title_main'] = array(
      '#type' => 'textfield',
      '#title' => t('Main Title'),
      '#size' => '30',
      '#description' => t('Uses the larger title font on the timeline'),
      '#default_value' => $this->options['title_main'],   
    );*/
  } 
  
  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */
	/*  
  function options_form(&$form, &$form_state) {
    $form['format'] = array(
      '#type' => 'radios',
      '#title' => t('JSON data format'),
      '#options' => array('Exhibit' => t('MIT Simile/Exhibit'), 'Canonical' => t('Canonical'), 'JSONP' => t('JSONP')),
      '#default_value' => $this->options['format'],
    );
  }
  */
  
}