(function ($) {
	Drupal.behaviors.viewsCalendarGallery = {
	  attach: function (context, settings) {
		  $('#calendar').fullCalendar({
			    events: JSON.parse( Drupal.settings.views_calendar_gallery ),
			    header: {
			        left: 'prev,next today',
			        center: 'title',
			        right: 'month,agendaWeek,agendaDay'
			    },
				defaultDate: '2017-02-12',
				defaultView: 'month',
			});
	  }
	};
})(jQuery);
