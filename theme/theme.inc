<?php
/**
 * Display the simple view of rows one after another
 */
function template_preprocess_views_calendar_gallery_style(&$vars) {

	drupal_add_js(drupal_get_path('module', 'views_calendar_gallery') .'/includes/jquery-ui/jquery-ui.min.js', 'file');
	drupal_add_js(drupal_get_path('module', 'views_calendar_gallery') .'/includes/moment/moment.js', 'file');
	drupal_add_js(drupal_get_path('module', 'views_calendar_gallery') .'/includes/fullcalendar/fullcalendar.min.js', 'file');
	drupal_add_css(drupal_get_path('module', 'views_calendar_gallery') .'/includes/fullcalendar/fullcalendar.min.css', 'file');
	drupal_add_css(drupal_get_path('module', 'views_calendar_gallery') .'/includes/fullcalendar/fullcalendar.print.css', 'file');
	drupal_add_js(drupal_get_path('module', 'views_calendar_gallery') .'/includes/fullcalendar/views_calendar_gallery.js', 'file');

  $view = $vars['view'];
  $results = $vars['view']->result;

	$arr_js_calendar = array();
	foreach( $results as $key => $result ) {
		$entity_result = $result->_entity_properties;
		foreach( $entity_result as $k => $value ) {
			if( $k == 'search_api_relevance' || $k == 'search_api_excerpt' || $k == 'search_api_id' ) {
				continue;
			}
			if( $k == 'field_dates:value' ) {
				$js_calendar['start'] = date( 'Y-m-d', $value );
//				$js_calendar['end'] = '';
			}
			if( $k == 'title' ) {
				$js_calendar['title'] = $value;
			}
		}
		$arr_js_calendar[] = $js_calendar;
	}
	drupal_add_js(array(
	  'views_calendar_gallery' => json_encode( $arr_js_calendar ),
	), 'setting');
}
